<?php

namespace App\Controller;
//use App\Entity\EcoleDoctorale;
//use App\Entity\Theses;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController//héritage de ma classe HommeController à la classe AbstractController
{
    /**
     * @Route("/home", name="home")//Route: c'est une Annotation Route(Quant un navigateur appelle mon site.com/home cette annotation va appeller la fonction index ci_desous)
     */
    public function index()//cette fonction va traiter la demande) et elle va nous renvoyer le fichier index.html.twig qui se trouve dans le dossier home dans le template
    {
       /*  $entityManager = $this->getDoctrine()->getManager();
        $ecoleDoctoraleRepository= $entityManager->getRepository(EcoleDoctorale::class);
        $ecoleDoctorale= $ecoleDoctoraleRepository->findAll();

        if(empty($ecoleDoctorale)){
            $ecole1 = new  EcoleDoctorale ();
            $ecole1->setNom("UniversitéMontpellier");
            $entityManager->persist($ecole1);

            $these1 = new Theses();
            $these1->setTitre("Climat");
            $these1->setDescription("réchauffement climatique");
            $these1->setEcoleDoctorale($ecole1);
            $entityManager->persist($these1);

            $these2 = new Theses();
            $these2->setTitre("Climat2");
            $these2->setDescription("réchauffementClimatique2");
            $these2->setEcoleDoctorale($ecole1);
            $entityManager->persist($these2);

            $entityManager->flush();

        }
        */



        return $this->render('home/index.html.twig', [

            //'ecoleDoctorales'=>$ecoleDoctoraleRepository->findAll(), 
            
            'controller_name' => 'HomeController',
            'infothese' =>  [

                
                ['titre'=>'Compression par profilage du code Java compilé pour les systèmes embarqués','contact'=>'saadanassim12@yahoo.fr','Description' =>'Les travaux de recherche code Java compilé pour les systèmes embarqués'], 
           
                ['titre'=>'Diagnostic multiple des systèmes complexes à base de réseaux bayésiens ','contact'=>'nassim.saada@etu.umontpellier.fr', 'Description' => 'L objectif de cette thése est de decrire des systèmes complexes à base de réseaux bayésiens '],
            
                [ 'titre'=> 'Modélisation et résolution de problèmes liés à l"exploitation d"infrastructures ferroviaires','contact'=> 'saadanassim12@yahoo.fr','Description' => 'L objectif de ce travail de résolution de problèmes liés à l"exploitation d"infrastructures ferroviaires']


            ]
        ]);
    }
}
